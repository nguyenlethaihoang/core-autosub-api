import pysrt
from deep_translator import DeepL

def translate_srt_file(input_file, output_file, src_lang='en', dest_lang='vi'):
    # Tải file SRT
    subs = pysrt.open(input_file, encoding='utf-8')

    # Dịch từng phụ đề và giữ nguyên thời gian hiển thị
    for sub in subs:
        try:
            translated_text = DeepL(api_key='your-api-key', source=src_lang, target=dest_lang).translate(sub.text)
            sub.text = translated_text
        except Exception as e:
            print(f"Error translating subtitle: {e}")

    # Lưu phụ đề đã dịch sang file mới
    subs.save(output_file, encoding='utf-8')

# Thay đổi các giá trị này để dịch file SRT của bạn
input_file = 'input.srt'
output_file = 'example_translated_vi.srt'

translate_srt_file(input_file, output_file)
