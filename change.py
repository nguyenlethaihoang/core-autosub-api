# Mảng chứa các từ cần thay thế

mang_thay_the = ['service', 'ID', 'data', 'certification', 'login', 'verify', 'certificate', 'prototype', 'features', 'blockchain', 'link', 'links', 'backend', 'reverse', 'performance','module', 'backup', 'memory', 'server', 'servers', 'file', 'static', 'public', 'private', 'proxy', 'processors', 'processor', 'module', 'modules', 'client', 'side', 'cache', 'caching', 'load', 'balancing', 'balancer', 'certificates']

# Tên file cần thay thế từ		
file_goc = 'good-sub.txt'
file_moi = 'change.srt'

# Đọc nội dung của file
with open(file_goc, 'r', encoding='utf-8') as f:
    noi_dung = f.read()

# Thực hiện thay thế các từ trong nội dung
noi_dung_moi = noi_dung.format(*mang_thay_the)

# Ghi nội dung mới vào file mới
with open(file_moi, 'w', encoding='utf-8') as f:
    f.write(noi_dung_moi)

print(f'Đã thay thế các từ trong {file_goc} và lưu vào {file_moi}.')
