import pysrt
from googletrans import Translator, LANGUAGES

def translate_srt_file(input_file, output_file, src_lang='en', dest_lang='vi'):
    # Đảm bảo rằng ngôn ngữ nguồn và đích hợp lệ
    if src_lang not in LANGUAGES or dest_lang not in LANGUAGES:
        raise ValueError("Invalid source or destination language")

    # Tải file SRT
    subs = pysrt.open(input_file, encoding='utf-8')

    # Tạo một đối tượng dịch
    translator = Translator()

    # Dịch từng phụ đề và giữ nguyên thời gian hiển thị
    for sub in subs:
        translated_text = translator.translate(sub.text, src=src_lang, dest=dest_lang).text
        sub.text = translated_text

    # Lưu phụ đề đã dịch sang file mới
    subs.save(output_file, encoding='utf-8')

# Thay đổi các giá trị này để dịch file SRT của bạn
input_file = 'input.srt'
output_file = 'input_translated_vi.srt'

translate_srt_file(input_file, output_file)
