from moviepy.editor import VideoFileClip, CompositeVideoClip, ColorClip

input_video_path = "1.Introduction.mp4"
output_video_path = "scaled_video.mp4"

# Load input video
input_clip = VideoFileClip(input_video_path)

# Scale video to 15%
scaled_clip = input_clip.resize(0.80)

# Calculate new frame size with black background
new_width = input_clip.size[0]
new_height = input_clip.size[1]
black_bg = ColorClip(size=(new_width, new_height), color=(0, 0, 0), duration=input_clip.duration)

# Calculate position of the scaled video
x_pos = (new_width - scaled_clip.size[0]) // 2
y_pos = 0

# Combine scaled video with black background
final_clip = CompositeVideoClip([black_bg.set_duration(input_clip.duration), scaled_clip.set_position((x_pos, y_pos)).set_duration(input_clip.duration)])

# Export the result
final_clip.write_videofile(output_video_path, codec='libx264', audio_codec='aac')
