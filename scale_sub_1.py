from moviepy.editor import *
import pysubs2
from moviepy.video.tools.subtitles import SubtitlesClip

def generator(txt):
    return TextClip(txt, font='Arial', fontsize=24, color='white')

input_subtitle_path = "1.Introduction.srt"
input_video_path = "scaled_video.mp4"
output_video_path = "output.mp4"

# Load subtitles from .srt file
subs = pysubs2.load(input_subtitle_path, format_='srt')  # Specify the format as 'srt'

# Convert pysubs2 subtitles to moviepy subtitles format
moviepy_subs = [((s.start / 1000, s.end / 1000), s.text) for s in subs]

subtitles = SubtitlesClip(moviepy_subs, generator)

video = VideoFileClip(input_video_path)
result = CompositeVideoClip([video, subtitles.set_pos(('center', 'bottom'))])

result.write_videofile(output_video_path, codec='libx264', audio_codec='aac')
