import pysubs2
from moviepy.editor import VideoFileClip, TextClip, CompositeVideoClip, ColorClip

input_video_path = "scaled_video.mp4"
input_subtitle_path = "1.Introduction.srt"
output_video_path = "output_video_with_subtitles.mp4"

def create_subtitle_clips(subtitles, font='Arial', fontsize=24, color='white', margin_bottom=30, stroke_color='black', stroke_width=1):
    clips = []
    for sub in subtitles:
        txt_clip = TextClip(sub.text, fontsize=fontsize, color=color, font=font, size=input_clip.size, stroke_color=stroke_color, stroke_width=stroke_width)
        start_seconds = sub.start / 1000
        end_seconds = sub.end / 1000
        txt_clip = txt_clip.set_position(('center', input_clip.size[1] - fontsize - margin_bottom)).set_start(start_seconds).set_end(end_seconds)
        
        # Add black background for better visibility
        bg_clip = ColorClip(size=(input_clip.w, txt_clip.h + 10), color=(0, 0, 0))
        bg_clip = bg_clip.set_position(('center', input_clip.size[1] - fontsize - margin_bottom)).set_start(start_seconds).set_end(end_seconds)
        
        clips.extend([bg_clip, txt_clip])
    return clips

# Load input video
input_clip = VideoFileClip(input_video_path)

# Load subtitles from .srt file
subs = pysubs2.load(input_subtitle_path, format_='srt')  # Specify the format as 'srt'
subtitle_clips = create_subtitle_clips(subs)

# Combine video and subtitles
final_clip = CompositeVideoClip([input_clip] + subtitle_clips)

# Export the result
final_clip.write_videofile(output_video_path, codec='libx264', audio_codec='aac')
