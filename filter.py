input_file_path = "./1.Introduction.srt"
output_file_path = "filter.srt"
word_list = ['service', 'ID', 'data', 'certification', 'login', 'verify', 'certificate', 'prototype', 'features', 'blockchain', 'link', 'links', 'backend', 'reverse', 'performance','module', 'backup', 'memory', 'server', 'servers', 'file', 'static', 'public', 'private', 'proxy', 'processors', 'processor', 'module', 'modules', 'client', 'side', 'cache', 'caching', 'load', 'balancing', 'balancer', 'certificates']

with open(input_file_path, "r") as input_file:
    input_text = input_file.read()

# Xử lý đoạn văn bằng cách tương tự như câu hỏi trước
modified_text = input_text
for word in word_list:
    modified_text = modified_text.replace(word, "{" + str(word_list.index(word)) + "}")

with open(output_file_path, "w") as output_file:
    output_file.write(modified_text)